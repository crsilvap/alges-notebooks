import sys

import matplotlib.pyplot as plt
import numpy as np

sys.path.append('../aster-loader/')
sys.path.append("/home/canas/Repositories/Github/bhtsne")
import bhtsne
from image import AsterImage


aster = AsterImage('../Data/hiperespectral/pg-PR1B0000-2001022302_022_001.h5')
cube = aster.load_band_cube('TIR')
flattened_cube = np.reshape(cube, (cube.shape[0]*cube.shape[1], cube.shape[2]))
embedding_array = bhtsne.run_bh_tsne(flattened_cube, initial_dims=flattened_cube.shape[1])

print(embedding_array.shape)
